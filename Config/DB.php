<?php


class DB extends PDO
{
    private $host = '148.226.12.16'; //	148.226.12.16
    private $database = 'cuo-espaciospublicos'; //cuo-espaciospublicos
    private $user = 'usrespapub'; //usrespapub
    private $password = 'ARq6Po3QMcUo'; //ARq6Po3QMcUo
    private $pdo;
    private $query;
    private $bConnected = false;
    private $parameters;

    /**
     * DB constructor.
     */

    public function __construct()
    {
        $dns = 'mysql:dbname=' . $this->database . ';host=' . $this->host;
        parent::__construct($dns, $this->user, $this->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    }

    public  function connection()
    {
        $dns = 'mysql:dbname=' . $this->database . ';host=' . $this->host;
        try {

            $this->pdo = new PDO($dns, $this->user, $this->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);


            return $this->pdo;
        } catch (PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }

    public function closeConnecion()
    {
        $this->pdo = null;
    }
}
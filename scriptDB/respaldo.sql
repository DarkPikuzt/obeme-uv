/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 5.7.24 : Database - obeme_encuesta
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`obeme_encuesta` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `obeme_encuesta`;

/*Table structure for table `cuo_obe_password` */

DROP TABLE IF EXISTS `cuo_obe_password`;

CREATE TABLE `cuo_obe_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` char(10) DEFAULT NULL,
  `universidad` char(150) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `cuo_obe_password` */

insert  into `cuo_obe_password`(`id`,`password`,`universidad`) values 
(2,'1234',NULL),
(3,'1478',NULL),
(4,'1458',NULL),
(5,'1454',NULL),
(6,'1238',NULL),
(7,'8596',NULL),
(8,'1569',NULL),
(9,'0000',NULL),
(10,'1459',NULL),
(11,'9632',NULL),
(12,'0002',NULL),
(13,'0005',NULL),
(14,'0006','patito'),
(15,'0007','patito'),
(16,'0009','patito');

/*Table structure for table `cuo_obe_usuario` */

DROP TABLE IF EXISTS `cuo_obe_usuario`;

CREATE TABLE `cuo_obe_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matricula` char(60) DEFAULT NULL,
  `fecha_ingreso` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

/*Data for the table `cuo_obe_usuario` */

insert  into `cuo_obe_usuario`(`id`,`matricula`,`fecha_ingreso`) values 
(1,'zs12345678',NULL),
(2,'zs12345678',NULL),
(3,'zs12345678','2020-04-30 12:58:01'),
(4,'zs12345678','2020-04-30 12:58:42'),
(5,'zs12345678','2020-04-30 12:59:02'),
(6,'zs12345678','2020-04-30 13:07:15'),
(7,'','2020-04-30 13:08:50'),
(8,'zs12345678','2020-04-30 13:10:57'),
(9,'zs12345678','2020-04-30 13:16:54'),
(10,'zs12345678','2020-04-30 13:18:07'),
(11,'zs12345678','2020-04-30 13:19:16'),
(12,'','2020-04-30 13:25:48'),
(13,'zs12345678','2020-04-30 13:28:39'),
(14,'zs12345','2020-04-30 13:28:58'),
(15,NULL,'2020-04-30 16:19:42'),
(16,'1234','2020-04-30 16:23:13'),
(17,'1234','2020-04-30 16:26:56'),
(18,'1234','2020-04-30 16:31:46'),
(19,'zs12345678','2020-04-30 16:38:18'),
(20,'zs12345678','2020-04-30 16:38:43'),
(21,'z','2020-04-30 16:38:49'),
(22,'zs','2020-04-30 16:39:19'),
(23,'zs12345678','2020-04-30 17:05:42'),
(24,'zs12345678','2020-04-30 17:19:53'),
(25,'zs12345678','2020-04-30 17:40:21'),
(26,'zs12345678','2020-04-30 17:52:39'),
(27,'zs12345678','2020-04-30 18:10:10'),
(28,'0006','2020-04-30 18:21:13'),
(29,'0007','2020-04-30 18:21:33'),
(30,'0007','2020-04-30 18:22:13'),
(31,'0009','2020-04-30 18:23:05'),
(32,'0009','2020-04-30 18:24:31'),
(33,'0009','2020-04-30 18:25:18'),
(34,'zs12345678','2020-04-30 18:26:39'),
(35,'0009','2020-04-30 18:28:19'),
(36,'0009','2020-04-30 18:32:20'),
(37,'0007','2020-04-30 18:37:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

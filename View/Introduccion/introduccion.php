<section class="introduccion">
    <div class="container">
        <div class="row">
            <div class="col-12 text-justify">
                <h4 class="text-center">
                    OBSERVATORIO DE EDUCACIÓN MÉDICA Y DERECHOS HUMANOS
                </h4>
                <h5 class="text-center">SEGUIMIENTO DE INTERNADO MÉDICO</h5>
                <p>La reincorporación de las y los internos de medicina a las unidades médicas de México durante la pandemia de Covid-19 se realiza bajo condiciones que resguardan su dignidad y sus derechos humanos (a la salud y a la educación, entre
                    otros). El objetivo de la siguiente encuesta es ofrecer a las y los internos un medio mediante el cual puedan informar sobre el cumplimiento de dichas condiciones, y así poder documentar y apoyar el cumplimiento de las mismas.
                </p>
                <p>La encuesta es semanal, con corte cada jueves a las 24:00 horas; se puede contestar semanalmente mientras esté vigente; es voluntaria, anónima y no representa ningún riesgo para quien la conteste.
                </p>
                <p>Si desea contribuir continúe, de lo contrario le agradecemos su tiempo y atención</p>
            </div>
            <div class="col-12">
                <a name="" id="" class="btn btn-primary" href="selepcion.php" role="button">Siguiente</a>
            </div>
        </div>
    </div>
</section>

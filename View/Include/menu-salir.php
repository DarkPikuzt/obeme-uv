
<style>
    .bg-menu li a{
        text-decoration: none;
        color: white;
        font-size: 1rem;
    }
    .bg-menu li{
        list-style: none;
        font-size: 1rem;
    }

    .text-uv{
        text-align: end;
    }


</style>

<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 bg-menu-principal">
                <strong>Universidad Veracruzana</strong>
            </div>
            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 bg-menu text-uv">
                <li><a href="salir.php">Salir<i class="fas fa-door-open"></i></a></li>

            </div>
        </div>
    </div>
</header>

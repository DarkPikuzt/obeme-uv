<style>
    .uv{
        width: 72% !important;
        position: relative;
        left: 15%;
    }

    .paddin-uv{
        padding: 1rem;

    }

    .item{
        position: relative;
        left: 10%;

    }

    @media (max-width: 540px) {
        .item{
        }
    }

</style>

<section class="introduccion">
    <div class="container">
        <div class="row  ">







            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 text-center paddin-uv item">
                <div class="card text-center" style="width: 18rem;">
                    <img src="./img/uv.png" class="card-img-top " alt="..." />
                    <div class="card-body">
                        <p class="card-text">
                        <h5 class="card-title text-center">Internos de la Universidad Veracruzana
                        </h5>
                        <div class="col-12 text-center">
                            <a name="" id="" class="btn btn-primary" href="uv.php" role="button">Entrar</a>
                        </div>
                        </p>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6  paddin-uv item">
                <div class="card" style="width: 18rem;">
                    <img src="./img/externa.png" class="card-img-top img-fluid uv " alt="..." />
                    <div class="card-body">
                        <p class="card-text">
                        <h5 class="card-title text-center">Internos de otras universidades del país

                        </h5>
                        <div class="col-12 text-center">
                            <a name="" id="" class="btn btn-primary" href="registro-externo.php" role="button">Entrar</a>
                        </div>
                        </p>
                    </div>
                </div>
            </div>



        </div>
    </div>
</section>

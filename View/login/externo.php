<style>
    /*estilos de login Interno*/

    .interno-uv {
        position: relative;
        top: 50px;
    }

    .borde-uv-login {
        box-shadow: 0 2px 6px #b6b6b6;
        border-width: 0;
        border-radius: 2px;
        width: 100%;
        margin: 0 8px 8px 0;
        padding: 16px 16px 8px 16px !important;
        position: relative;
        top: 25%
    }

    .uv{
        max-width: 75%;
    }
</style>

<section class="interno-uv">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <img src="./img/externa.png" alt="" class="img-fluid uv" srcset="">
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                <div class="borde-uv-login">
                    <form id="user_session" >
                        <fieldset>
                            <div id="Error"></div>
                            <h4 class="text-left" style="font-family:'Arial','Open Sans';">Iniciar Sesión</h4>
                            <hr>
                            <div class="form-group" id="userDiv">
                                <input type="text" id="username" name="username" class="form-control"
                                       placeholder="Ingrese Matricula" name="username" autofocus="" required="">
                            </div>
                        </fieldset>
                    </form>
                    <button type="button" class="btn btn-primary" id="ingresar" value="Registrar"> Ingresar
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>


<script>
    $(document).ready(function(){
        $('#ingresar').on('click', function (event) {
            event.preventDefault();
            let muestra = $('#username').val();
            let numero = /^[0-9]$/;
            let array = Array.from(muestra);
            let datos = $('#user_session').serialize();
            let url = "Controller/cUserExternoLogin.php";

            let bandera = false;

            if(array.length  > 4){
                $('#Error.has-error').removeClass('alert alert-danger');
                $('.help-block').remove();
                $('#Error').addClass('has-error');
                $('#Error').append('<div class=" alert alert-danger help-block"> <strong>Contraseña no debe ser mayor a 4 caracteres </strong> </div>');
                return false;

            }else{
                $('#Error.has-error').removeClass('has-error');
                $('.help-block').remove();


                $.ajax({
                    type: "POST",
                    data: datos,
                    url: url,
                    success: function (data) {

                        console.log(data);
                        if (data === '1'){
                            window.location.href = "encuesta.php";
                        }

                        if (data != '1'){
                            $('#Error.has-error').removeClass('alert alert-danger');
                            $('.help-block').remove();
                            $('#Error').addClass('has-error');
                            $('#Error').append('<div class=" alert alert-danger help-block"> <strong>Esta Contraseña ya esta registrada en la base de datos </strong> </div>');
                            return false;
                        }
                    }
                });
            }




            //console.log(datos);





        });
    });
</script>




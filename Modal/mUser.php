<?php

include_once '../Config/DB.php';


class mUser
{

    public $enrollment;
    public $password;
    public $universidad;


    public function getUniversidad()
    {
        return $this->universidad;
    }


    public function setUniversidad($universidad)
    {
        $this->universidad = $universidad;
    }




    public function getPassword()
    {
        return $this->password;
    }


    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getEnrollment()
    {
        return $this->enrollment;
    }


    public function setEnrollment($enrollment)
    {
        $this->enrollment = $enrollment;
    }


    public function addUserExterno(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $nameEnrollment = $this->getPassword();
        $date = $this->CreateDate();
        //var_dump($nameCategory);

        try{
            $sql = "INSERT INTO cuo_obe_usuario (matricula,fecha_ingreso) VALUES (?,?)";
            $query = $conn->prepare($sql);
            $query->bindParam(1,$nameEnrollment);
            $query->bindParam(2,$date);
            $result = $query->execute();
            //var_dump($result);

            $this->AsignarUserExterno();

            return $result;
        }catch (PDOException $e){
            echo $e->getMessage();
            die();
        }

    }

    public function adduser(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $nameEnrollment = $this->getEnrollment();
        $date = $this->CreateDate();
        //var_dump($nameCategory);

        try{
            $sql = "INSERT INTO cuo_obe_usuario (matricula,fecha_ingreso) VALUES (?,?)";
            $query = $conn->prepare($sql);
            $query->bindParam(1,$nameEnrollment);
            $query->bindParam(2,$date);
            $result = $query->execute();
            //var_dump($result);
            $this->AsignarUser();

            return $result;
        }catch (PDOException $e){
            echo $e->getMessage();
            die();
        }
    }

    public function CreateDate(){
        date_default_timezone_set('America/mexico_city');
        //$date =  date();
        return date('Y-m-d H:i:s');
    }

    public function AsignarUser(){
        $_SESSION['Usuario'] = $this->getEnrollment();
    }

    public function AsignarUserExterno(){
        $_SESSION['Usuario'] = $this->getPassword();
    }


    public function addPassword(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $password = $this->getPassword();
        $universidad = $this->getUniversidad();


        try{
            $sql = "INSERT INTO cuo_obe_password (password,universidad) VALUES (?,?)";
            $query = $conn->prepare($sql);
            $query->bindParam(1,$password, PDO::PARAM_STR);
            $query->bindParam(2,$universidad);
            $result = $query->execute();



            $this->AsignarUser();
            return $result;
        }catch (PDOException $e){
            echo $e->getMessage();
            die();
        }
    }

    public function repeatPassword(){
        $conexion = new DB();
        $conn = $conexion->connection();
        $password = $this->getPassword();

        //var_dump($password);


        try{
            $sql ="select * from cuo_obe_password cop where cop.password = '$password' ";
            $query = $conn->query($sql);
            $result = $query->fetchColumn();
            return $result;

        }catch (PDOException $e){
            echo $e->getMessage();
            die();
        }

    }

}